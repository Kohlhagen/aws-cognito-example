var AWS = require('aws-sdk');
var AmazonCognitoIdentity = require('amazon-cognito-identity-js');
global.fetch = require('node-fetch');


var authenticationData = {
  Username : '<YOUR_USERNAME>',
  Password : '<YOUR_PASSWORT>',
};
var authenticationDetails = new AmazonCognitoIdentity.AuthenticationDetails(authenticationData);
var poolData = { UserPoolId : '<YOUR_UserPoolId>', // AWS KONSOLE-> cog. -> Benutzerpool -> Allegemeine Einstellungen
  ClientId : '<YOUR_ClientId>'  /// AWS KONSOLE-> cog. -> Benutzerpool --> App Integration -> App Client Einstellungn //Note: When creating the app, make sure that no Clint Secret is hacked on.
};
var userPool = new AmazonCognitoIdentity.CognitoUserPool(poolData);
var userData = {
  Username : '<YOUR_USERNAME> ',
  Pool : userPool
};
var cognitoUser = new AmazonCognitoIdentity.CognitoUser(userData);


cognitoUser.authenticateUser(authenticationDetails, {
  onSuccess: function (result) {
    var accessToken = result.getAccessToken().getJwtToken();
    console.log("accessToken");
    console.log(accessToken);

    var idToken = result.idToken.jwtToken;
    console.log("Sende diesen Token zum API-Gateway im Header Authorization");
    console.log(idToken);
    
  },

    onFailure: function (err) {
        console.log(err);
    },

  mfaRequired: function(codeDeliveryDetails) {
      cognitoUser.sendMFACode(mfaCode, this)
  },

  newPasswordRequired: function(userAttributes, requiredAttributes) {
      delete userAttributes.email_verified;
      cognitoUser.completeNewPasswordChallenge(newPassword, userAttributes, this);
  },

  onFailure: function(err) {
    console.log(err);
    console.log(err.message);
  }

});